//Valeurs de base
let actualSeason;
let actualStat = "breakBlocks";
let actualSearch = "";
let nbElement = 100;
let numeroPage = 1;
let numeroPageTotal = 0;

//uuid
var url = window.location.href;
var queryString = url.split("?")[1];
var params = queryString.split("&");
for (var i = 0; i < params.length; i++) {
  var keyValuePair = params[i].split("=");
  if (keyValuePair[0] == "uuid") {
    var uuid = keyValuePair[1];
    break;
  }
}

//Si pas de uuid, on retourne à la page d'accueil
if (uuid == "" || uuid == undefined) window.location.href = "./index.html";

//Titre
let div = document.getElementById("title");
fetch(`http://193.70.40.178:8080/api/v1/info/pseudo/${uuid}`)
  .then(function (response) {
    return response.json();
  })
  .then(function (data) {
    div.innerHTML = `Profil de ${data}`;
  });

//Dernier reload
function lastReload() {
  fetch(`http://193.70.40.178:8080/api/v1/info/${actualSeason}/lastUpdate`)
    .then(function (response) {
      return response.json();
    })
    .then(function (data) {
      div = document.getElementById("lastReload");
      div.innerHTML = data;
    });
}

//Fonction pour générer la pagination en bas du tableau
function pagination() {
  const paginations = document.querySelectorAll(".pagination-numerique");
  paginations.forEach((pagination) => {
    pagination.innerHTML = "";
    //Fleche précédente
    pagination.innerHTML += `<button class="previous"><</button> `;
    //Avant Actual 1
    if (numeroPage > 2)
      pagination.innerHTML += `<button class="numeric-button">1</button> `;
    if (numeroPage > 3) pagination.innerHTML += `... `;
    //Cas des 4 dernières pages
    if (numeroPage - 2 > 0 && numeroPage === numeroPageTotal)
      pagination.innerHTML += `<button class="numeric-button">${
        numeroPage - 3
      }</button> `;
    if (
      numeroPage - 2 > 0 &&
      (numeroPage === numeroPageTotal || numeroPage === numeroPageTotal - 1)
    )
      pagination.innerHTML += `<button class="numeric-button">${
        numeroPage - 2
      }</button> `;
    //Avant Actual 2
    if (numeroPage > 1)
      pagination.innerHTML += `<button class="numeric-button">${
        numeroPage - 1
      }</button> `;
    //Actual
    pagination.innerHTML += `<button class="actual">${numeroPage}</button> `;
    //Page suivante
    if (numeroPage + 1 < numeroPageTotal)
      pagination.innerHTML += `<button class="numeric-button">${
        numeroPage + 1
      }</button> `;
    //Cas des 4 premières pages
    if (
      numeroPage + 2 < numeroPageTotal &&
      (numeroPage === 1 || numeroPage === 2)
    )
      pagination.innerHTML += `<button class="numeric-button">${
        numeroPage + 2
      }</button> `;
    if (numeroPage + 3 < numeroPageTotal && numeroPage === 1)
      pagination.innerHTML += `<button class="numeric-button">${
        numeroPage + 3
      }</button> `;
    //Après actual
    if (numeroPage + 2 < numeroPageTotal) pagination.innerHTML += `... `;
    if (numeroPage < numeroPageTotal)
      pagination.innerHTML += `<button class="numeric-button">${numeroPageTotal}</button> `;
    //Fleche suivante
    pagination.innerHTML += `<button class="next">></button> `;
  });

  const buttons = document.querySelectorAll(".numeric-button");
  buttons.forEach((button) => {
    button.addEventListener("click", function (event) {
      numeroPage = parseInt(event.target.innerHTML);
      updateStatSection(actualSeason, actualStat);
    });
  });

  const next = document.querySelectorAll(".next");
  next.forEach((button) => {
    button.addEventListener("click", function () {
      if (numeroPage + 1 <= numeroPageTotal) {
        numeroPage++;
        updateStatSection(actualSeason, actualStat);
      }
    });
  });

  const previous = document.querySelectorAll(".previous");
  previous.forEach((button) => {
    button.addEventListener("click", function () {
      if (numeroPage - 1 > 0) {
        numeroPage = numeroPage - 1;
        updateStatSection(actualSeason, actualStat);
      }
    });
  });
}

//Fonction pour set les données dans la table des stats
function updateStatSection() {
  let url = `http://193.70.40.178:8080/api/v1/${actualStat}/${actualSeason}/${uuid}/${actualSearch}`;
  fetch(url)
    .then(function (response) {
      div = document.querySelector(".spinner-border");
      div.setAttribute("style", "display: show;");
      div = document.getElementById("TableStatSection");
      div.setAttribute("style", "display: none;");
      return response.json();
    })
    .then(function (data) {
      numeroPageTotal = Math.ceil(data[0].length / 100);

      div = document.getElementById("TableStatSection");
      div.innerHTML = "";
      for (
        let i = 0 + (numeroPage - 1) * nbElement;
        i < data[0].length && i < numeroPage * nbElement;
        i++
      ) {
        if (data[0][i] !== "pseudo" && data[0][i] !== "uuid") {
          //https://mc.nerothe.com/
          if (actualStat.includes("Blocks") || actualStat.includes("Items")) {
            div.innerHTML += `<tr> <td> <img class="minecraft_png" src="./blocks_items/${convertItemName(
              data[0][i]
            )}.png"> </img> </td> <td> <span> ${
              data[0][i]
            } </span> </td> <td> <span> ${data[1][i]} </span> </td> </tr>`;
          } else {
            if (data[0][i].includes("_ONE_CM"))
              div.innerHTML += `<tr> <td> <span> ${
                data[0][i].substring(0, data[0][i].length - 6) + "DISTANCE"
              } </span> </td> <td> <span> ${
                Math.round(data[1][i] / 100) + " m"
              } </span> </td> </tr>`;
            else if (data[0][i].includes("PLAY_ONE_MINUTE"))
              div.innerHTML += `<tr> <td> <span> ${"PLAY_TIME"} </span> </td> <td> <span> ${
                Math.round((data[1][i] / (3600 * 20) / 24) * 100) / 100 + " j"
              } </span> </td> </tr>`;
            else
              div.innerHTML += `<tr> <td> <span> ${data[0][i]} </span> </td> <td> <span> ${data[1][i]} </span> </td> </tr>`;
          }
        }
      }
    })
    .then(function () {
      div = document.querySelector(".spinner-border");
      div.setAttribute("style", "display: none;");
      div = document.getElementById("TableStatSection");
      div.setAttribute("style", "display: show;");
      pagination();
    });
}

function convertItemName(name) {
  let res = name.toLowerCase();
  return res;
}

//Ajout d'un événement pour le bouton confirmer
const button = document.getElementById("confirm");
button.addEventListener("click", function () {
  const season = document.getElementById("selectSeason");
  const stat = document.getElementById("selectStat");
  const search = document.getElementById("searchBar");

  numeroPage = 1;
  actualSearch = search.value;
  actualSeason = season.value;
  actualStat = stat.value;
  updateStatSection(season, stat);
  lastReload();

  const title = document.getElementById("TitleStatSection");
  title.innerHTML = document.getElementById(
    "selectStat-" + stat.value
  ).innerHTML;
});


//Valeurs dans le selecteur de saison
//+ Affichage des stats
const select = document.getElementById("selectSeason");
console.log(`http://193.70.40.178:8080/api/v1/info/allSeason/${uuid}`)
fetch(`http://193.70.40.178:8080/api/v1/info/allSeason/${uuid}`)
  .then(function (response) {
    return response.json();
  })
  .then(function (data) {
    select.innerHTML = "";
    actualSeason = data[data.length-1]
    for (let i = data.length - 1; i >= 0; i--) {
      select.innerHTML += `<option value="${data[i]}">${data[i]}</option>`;
    }
  }).then(function () {
    updateStatSection("breakBlocks");
    lastReload();
  })