# [EN COURS] Site Stats BC

Quentin BERNARD

## Description du projet

Le projet "Site Stats BC" est un ensemble de projets visant à faciliter la gestion des statistiques des joueurs dans un monde Minecraft. Il comprend un plugin Minecraft développé en Java, une API REST et un site web dédié à la visualisation des données.

Le premier volet du projet consiste en un plugin Minecraft en Java. Ce plugin permet d'importer les statistiques des joueurs ayant participé à un monde Minecraft dans une base de données. Les statistiques peuvent inclure des informations telles que le temps de jeu, les succès accomplis, les ressources collectées, les créatures tuées, etc. Grâce à ce plugin, les données des joueurs sont automatiquement enregistrées et mises à jour dans la base de données.

Le deuxième volet du projet est une API REST qui offre des fonctionnalités pour interroger la base de données contenant les statistiques des joueurs. Les utilisateurs peuvent faire des requêtes vers cette API pour récupérer les données spécifiques dont ils ont besoin. Par exemple, ils peuvent obtenir les statistiques d'un joueur particulier, les classements des meilleurs joueurs en fonction de certains critères, ou encore les données agrégées pour une période donnée. L'API REST facilite ainsi l'accès aux données et leur utilisation dans d'autres applications ou services.

Enfin, le troisième volet du projet est un site web dédié à la visualisation des statistiques des joueurs. Ce site permet aux utilisateurs de naviguer à travers les données importées depuis Minecraft, d'effectuer des recherches et de filtrer les résultats en fonction de leurs besoins. Dans le futur, le site "Site Stats BC" envisage également d'élargir ses fonctionnalités en fournissant d'autres informations utiles pour les joueurs, telles que des images des différentes maps Minecraft, des liens de téléchargement, etc.

Vidéo de présentation : lien
Lien du site : http://193.70.40.178/best-cube/

## Fonctionnalités

- Les statistiques sont toutes stockées sur une base de données hébergée sur mon VPS.
- Liste des joueurs par saison (seulement la saison 9 pour le moment).
- Lorsqu’on est sur le profil d’un joueur, on peut choisir de voir une catégorie de statistique pour une saison donnée.
- Navigation fluide entre les pages de statistiques
- Il y a des images qui correspondent aux blocs/items
- CSS approximatif afin de rendre le tout plus clair.

L'objectif ultime de ce projet est de créer une plateforme complète pour la gestion et la visualisation des statistiques Minecraft. En regroupant le plugin, l'API et le site web, "Site Stats BC" offre aux joueurs un moyen pratique d'analyser et d'utiliser les données de jeu, tout en proposant des fonctionnalités supplémentaires pour enrichir leur expérience Minecraft.

## Requêtes 

| URI | Opération | Réponse | 
| ---------------------------------------- | ------ | ---------------------------------------------------- |
| /info/{BCseason}/lastUpdate | GET | Date de la dernière update de la saison concerné |
| /playerList/{BCseason} | GET | Liste (uuid / pseudo) de tous les joueurs d'une saison |
| /breakBlocks/{BCseason}/{pseudo} | GET | Liste (bloc / nb cassé) du joueur pour une saison donné | 
| /breakTools/{BCseason}/{pseudo} | GET | Liste (outils / nb cassé) du joueur pour une saison donné | 
| /craftBlocks/{BCseason}/{pseudo} | GET | Liste (bloc / nb crafté) du joueur pour une saison donné | 
| /craftItems/{BCseason}/{pseudo} | GET | Liste (item / nb crafté) du joueur pour une saison donné | 
| /killedByMobs/{BCseason}/{pseudo} | GET | Liste (mobs / nb de fois tué par le mob) du joueur pour une saison donné | 
| /mobsKill/{BCseason}/{pseudo} | GET | Liste (mobs / nb de fois tué) du joueur pour une saison donné | 
| /other/{BCseason}/{pseudo} | GET | Liste (achievement / statistique) du joueur pour une saison donné | 
| /placeBcloks/{BCseason}/{pseudo} | GET | Liste (bloc / nb cassé) du joueur pour une saison donné | 

