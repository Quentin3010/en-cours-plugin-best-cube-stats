package me.jesuismister.commands;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

import me.jesuismister.Main;
import me.jesuismister.Other;
import net.md_5.bungee.api.ChatColor;

public class CommandsBCAdmin implements CommandExecutor {
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String msg, String[] args) {

		if (sender instanceof Player && args.length >= 1) {
			Player player = (Player) sender;
			if (args.length==1 && args[0].equalsIgnoreCase("where")) {
				Other.sound_accepter(player);
				where(player);
				return true;
			}else if (args[0].equalsIgnoreCase("broadcast")) {
				broadcast(player, args);
				return true;
			}else if (args.length==2 && args[0].equalsIgnoreCase("purge")) {
				purge(player, args);
				return true;
			}else if (args.length>=1 && args[0].equalsIgnoreCase("money")) {
				money(player, args);
				return true;
			}else if (args.length==1 && args[0].equalsIgnoreCase("help")) {
				Other.sound_action(player);
				help(player);
				return true;
			}else {
				Other.sound_refuser(player);
				sender.sendMessage(Other.bc + ChatColor.RED + "La commande n'est pas reconnu.");
			}
		}
		return false;
	}
	
	List<Entry<String, Double>> getPlayerBalance(){
		//Recup les données
		Map<String, Double> map = new HashMap<String, Double>();
		for(OfflinePlayer p : Bukkit.getOfflinePlayers()) {
			map.put(p.getName(), Main.getEconomy().getBalance(p));
		}
		
		//Trie les données
		List<Entry<String, Double>> list = new ArrayList<>(map.entrySet());
        list.sort(Entry.comparingByValue());
        
        return list;
	}
	
	@SuppressWarnings("deprecation")
	private void money(Player player, String[] args) {
		if(!Main.bddOn) {
			player.sendMessage(Other.bc + ChatColor.RED + "La connexion à la BDD au démarrage du serveur a échoué.");
			return;
		}
		
		double money;
		
		if(args.length==2 && args[1].equalsIgnoreCase("top")) {
			player.sendMessage(Other.bc + ChatColor.BLUE + "Voici le montat de tous les comptes en banque : ");
			for(Entry<String, Double> entry : getPlayerBalance()) {
				player.sendMessage(ChatColor.GRAY +  "- " + ChatColor.GOLD + entry.getKey() + ChatColor.DARK_GRAY + " ➤ " + ChatColor.YELLOW + Other.printBalance(entry.getValue()) + " €");
			}
			Other.sound_accepter(player);
			return;
		}else if(args.length==4) {
			if(!Main.getEconomy().hasAccount(args[2])) {
				player.sendMessage(Other.bc + ChatColor.RED + "Le joueur ne possède pas de compte en banque.");
				Other.sound_refuser(player);
				return;
			}
			try {
				money = Double.parseDouble(args[3]);
			}catch(Exception e){
				 money = 0;
			}
			if(money<=0) {
				player.sendMessage(Other.bc + ChatColor.RED + "Le montant n'est pas valide.");
				Other.sound_refuser(player);
				return;
			}else if(args[1].equalsIgnoreCase("give")) {
				Main.getEconomy().depositPlayer(args[2], money);
				player.sendMessage(Other.bc + ChatColor.GREEN + "Vous venez d'ajouter " + ChatColor.YELLOW + Other.printBalance(money) + " €" + ChatColor.GREEN + " sur le compte de " + args[2]);
				Other.sound_accepter(player);
				return;
			}else if(args.length==4 && args[1].equalsIgnoreCase("remove")) {
				if(money>Main.getEconomy().getBalance(args[2])) Main.getEconomy().withdrawPlayer(args[2], Main.getEconomy().getBalance(args[2]));
				else Main.getEconomy().withdrawPlayer(args[2], money);
				player.sendMessage(Other.bc + ChatColor.GREEN + "Vous venez de retirer " + ChatColor.YELLOW + Other.printBalance(money) + " €" + ChatColor.GREEN + " sur le compte de " + args[2]);
				Other.sound_accepter(player);
				return;
			}
		}
		player.sendMessage(Other.bc + ChatColor.RED + "Syntaxe : /bcadmin money top OU /bcadmin money <give/remove> <PSEUDO> <MONTANT>");
		Other.sound_refuser(player);
	}
	
	private void purge(Player player, String[] args) {
		if(args[1].equalsIgnoreCase("hostile")) PurgeHostile(player);
		else if(args[1].equalsIgnoreCase("item")) PurgePassif(player);
		else if(args[1].equalsIgnoreCase("passive")) PurgePassif(player);
		else if(args[1].equalsIgnoreCase("all")) PurgeAll(player);
		else {
			player.sendMessage(Other.bc + ChatColor.RED + "Syntaxe : /bcadmin purge <hostile/item/passive/all>");
			Other.sound_refuser(player);
			return;
		}
		Other.sound_accepter(player);
	}
	
	private String singleLinePurge(String type, int nbEntite) {
		return Other.bc + ChatColor.GREEN + "Toutes les entités \"" + type + "\" ont été détruite (" + nbEntite + ").";
	}
	
	private int PurgeHostile(Player player) {
		int nbEntite = 0;

		for (World world : Bukkit.getWorlds()) {
			for (Entity e : world.getEntities()) {
				if (e.getCustomName() == null && Other.estUnMosntre(e)) {
					nbEntite++;
					e.remove();
				}
			}
		}
		player.sendMessage(singleLinePurge("hostile", nbEntite));
		return nbEntite;
	}

	private int PurgeItem(Player player) {
		int nbEntite = 0;

		for (World world : Bukkit.getWorlds()) {
			for (Entity e : world.getEntities()) {
				if (e.getType().equals(EntityType.DROPPED_ITEM)) {
					nbEntite++;
					e.remove();
				}
			}
		}
		player.sendMessage(singleLinePurge("item", nbEntite));
		return nbEntite;
	}

	private int PurgePassif(Player player) {
		int nbEntite = 0;

		for (World world : Bukkit.getWorlds()) {
			for (Entity e : world.getEntities()) {
				if (e.getCustomName() == null && !Other.otherEntity(e) && !Other.estUnMosntre(e)) {
					nbEntite++;
					e.remove();
				}
			}
		}
		player.sendMessage(singleLinePurge("passive", nbEntite));
		return nbEntite;
	}

	private void PurgeAll(Player player) {
		int nbEntite = PurgeHostile(player) + PurgeItem(player) + PurgePassif(player);
		
		player.sendMessage(singleLinePurge("all", nbEntite));
	}
	
	private boolean where(Player sender) {
		Location sLoc = sender.getLocation();
		Location pLoc;
		
		sender.sendMessage(Other.bc + ChatColor.BLUE +  "Position de tous les joueurs sur le serveur :");
		for (World world : Bukkit.getWorlds()) {
			for (Player player : world.getPlayers()) {
				pLoc = player.getLocation();
				if (sender.getWorld().equals(player.getWorld())) {
					sender.sendMessage(singleLineCoords(world, player, pLoc, calculDistance(sLoc, pLoc)+""));
				} else {
					sender.sendMessage(singleLineCoords(world, player, pLoc, "?"));
				}
			}
		}
		return true;
	}
	
	private String singleLineCoords(World world, Player player, Location pLoc, String distance) {
		return ChatColor.GRAY + "- [" + ChatColor.RED + world.getName() + ChatColor.GRAY + "] " + ChatColor.GOLD + player.getDisplayName() + ChatColor.GRAY + " : " + ChatColor.AQUA + (int) pLoc.getX() + " " + (int) pLoc.getY() + " " + (int) pLoc.getZ() + ChatColor.GRAY + " (" + distance + ")";
	}
	
	public static int calculDistance(Location sLoc, Location pLoc) {
		int x = 0;
		int z = 0;
		// Calcul de la distance sur l'axe x
		if ((sLoc.getX() < 0 && pLoc.getX() < 0) || (sLoc.getX() >= 0 && pLoc.getX() >= 0)) {
			x = (int) ((int) Math.abs(sLoc.getX() - pLoc.getX()));
		} else {
			x = (int) ((int) Math.abs(sLoc.getX()) + Math.abs(pLoc.getX()));
		}
		// Calcul de la distance sur l'axe z
		if ((sLoc.getZ() < 0 && pLoc.getZ() < 0) || (sLoc.getZ() >= 0 && pLoc.getZ() >= 0)) {
			z = (int) ((int) Math.abs(sLoc.getZ() - pLoc.getZ()));
		} else {
			z = (int) ((int) Math.abs(sLoc.getZ()) + Math.abs(pLoc.getZ()));
		}
		// Petit pythagore toi mÂ§me tu sais
		return (int) (Math.sqrt(Math.pow(x, 2) + Math.pow(z, 2)));
	}
	
	private boolean broadcast(Player player, String[] args) {
		if(args.length<=1) {
			Other.sound_refuser(player);
			return false;
		}
		
		StringBuilder broadcast = new StringBuilder();
		
		args[0] = "";
		for (String arg : args) {
			broadcast.append(arg + " ");
		}
		Bukkit.broadcastMessage(Other.bc + ChatColor.DARK_RED + player.getName() + ChatColor.DARK_GRAY + " ➤ " + ChatColor.RED + broadcast.toString());
		
		Other.sound_accepter(player);
		return true;
	}
	
	private void help(Player player) {
		player.sendMessage(Other.bc +  ChatColor.BLUE + "Voici la liste des commandes admin du plugin Best Cube :");
		player.sendMessage(singleLineHelp("broadcast <MESSAEGE>", "Permet d'envoyer une annonce."));
		player.sendMessage(singleLineHelp("help", "Permet d'afficher l'aide aux commandes."));
		player.sendMessage(singleLineHelp("money <ACTION> <PSEUDO> <MONTANT>", "Permet de modifier le compte en banque d'un jour."));
		player.sendMessage(singleLineHelp("purge <TYPE>", "Permet de faire disparaitre des types d'entité."));
		player.sendMessage(singleLineHelp("where", "Permet de localiser tous les joueurs."));
	}
	
	private String singleLineHelp(String alias, String msg) {
		return ChatColor.GRAY + "- " + ChatColor.GOLD + "/bc " + alias + " " + ChatColor.DARK_GRAY + "➤ " + ChatColor.RESET + msg;
	}
	
	
}