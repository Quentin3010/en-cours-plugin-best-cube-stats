package me.jesuismister.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.time.Instant;
import java.util.Date;
import java.util.List;

public class Requetes {
	private static final String ANSI_RESET = "\u001B[0m";
	private static final String ANSI_RED = "\u001B[31m";

	public static void createTable(String tabName, String tabName2, List<String> colonnes, boolean sizeSmall) {
		/*
		 * int size = 10; if(sizeSmall==true) { size = 4; }
		 */
		try (Connection con = DS.getConnection()) {
			String query = "DROP TABLE IF EXISTS " + tabName + "_" + tabName2;
			PreparedStatement ps = (PreparedStatement) con.prepareStatement(query);
			ps.executeUpdate();

			query = "CREATE TABLE " + tabName + "_" + tabName2 + "(uuid TEXT, pseudo TEXT,`";
			for (String col : colonnes) {
				query += col + "` INTEGER,`";
			}
			query = query.substring(0, query.length() - 2) + ")";
			ps = (PreparedStatement) con.prepareStatement(query);
			ps.executeUpdate();

			con.close();
		} catch (Exception e) {
			System.out.println(
					ANSI_RED + "ERREUR : Impossible de creer la table " + tabName + "_" + tabName2 + ANSI_RESET);
			e.printStackTrace();
		}
	}

	public static void createTable(String tabName, String tabName2, List<String> colonnes) {
		createTable(tabName, tabName2, colonnes, false);
	}

	public static void executeRequete(String requete) {
		try (Connection con = DS.getConnection()) {
			PreparedStatement ps = (PreparedStatement) con.prepareStatement(requete);
			ps.executeUpdate();
			con.close();
		} catch (Exception e) {
			System.out.println(ANSI_RED + "ERREUR : " + requete + ANSI_RESET);
			e.printStackTrace();
		}
	}

	public static void setInfoTable(String BCSeason) {
		String query = "";
		try (Connection con = DS.getConnection()) {
			query = "CREATE TABLE IF NOT EXISTS _Informations(BCSeason VARCHAR(10) PRIMARY KEY, lastUpdate TEXT)";
			PreparedStatement ps = (PreparedStatement) con.prepareStatement(query);
			ps.executeUpdate();

			ps = (PreparedStatement) con.prepareStatement(
					"INSERT INTO _Informations VALUES('" + BCSeason + "', '" + Date.from(Instant.now())
							+ "') ON DUPLICATE KEY UPDATE lastUpdate = '" + Date.from(Instant.now()) + "';");
			ps.executeUpdate();

			con.close();
		} catch (Exception e) {
			System.out.println(ANSI_RED + "ERREUR : Lors de l'ajout/creation dans _Informartions" + ANSI_RESET);
			e.printStackTrace();
		}
	}
}
