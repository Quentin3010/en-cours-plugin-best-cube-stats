package me.jesuismister.sql;

import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;

public class Article {
	private String nom;
	private int quantiteTotalVendu;
	private double proportionDesVentes;

	public Article(String nom, int quantiteTotalVendu, double proportionDesVentes) {
		this.nom = nom.toUpperCase();
		this.quantiteTotalVendu = quantiteTotalVendu;
		this.proportionDesVentes = proportionDesVentes;
	}

	private String convertQuantiteTotalVendu(int quantiteTotalVendu) {
		if (quantiteTotalVendu < 1000)
			return quantiteTotalVendu + "";
		if (quantiteTotalVendu < 1000000)
			return Math.round(quantiteTotalVendu / 1000) + "k";
		return Math.round(quantiteTotalVendu / 1000000) + "M";
	}

	public String toTxt(int i) {
		if (this.proportionDesVentes < 20)
			return ChatColor.WHITE + "" + i + " - " + this.nom + " : "
					+ convertQuantiteTotalVendu(this.quantiteTotalVendu) + " (" + Math.round(this.proportionDesVentes)
					+ "%)";
		return ChatColor.RED + "" + i + " - " + this.nom + " : " + convertQuantiteTotalVendu(this.quantiteTotalVendu)
				+ " (" + Math.round(this.proportionDesVentes * 10) / 10 + "%)";
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getQuantiteTotalVendu() {
		return quantiteTotalVendu;
	}

	public void setQuantiteTotalVendu(int quantiteTotalVendu) {
		this.quantiteTotalVendu = quantiteTotalVendu;
	}

	public double getProportionDesVentes() {
		return proportionDesVentes;
	}

	public void setProportionDesVentes(double proportionDesVentes) {
		this.proportionDesVentes = proportionDesVentes;
	}

	public static boolean isTop5(List<Article> top5Ventes, Material mat) {
		for (Article a : top5Ventes) {
			if (a.getNom().equalsIgnoreCase(mat.name()) && a.getProportionDesVentes() >= 20)
				return true;
		}
		return false;
	}

}
