package me.jesuismister.sql;

import java.sql.Connection;
import java.sql.DriverManager;

import me.jesuismister.Main;

public class DS {
	private static final String ANSI_RESET = "\u001B[0m";
	private static final String ANSI_RED = "\u001B[31m";

	private static String url;
	private static String nom;
	private static String mdp;

	public static boolean setupDS(Main main) {
		try {
			url = "jdbc:mysql://" + main.getConfig().getString("bdd.ip") + "/"
					+ main.getConfig().getString("bdd.nom_base");
			nom = main.getConfig().getString("bdd.nom");
			mdp = main.getConfig().getString("bdd.mdp");

			if (url.equals("nom de l'utilisateur") || url.equals("mot de passe"))
				throw new Exception();

			// Installation du driver
			Class.forName("com.mysql.jdbc.Driver");

			return true;
		} catch (Exception e) {
			System.out.println(
					ANSI_RED + "Erreur lors de la mise en place de la relation avec la base de donnee." + ANSI_RESET);
			e.printStackTrace();
		}
		return false;
	}

	public static Connection getConnection() {
		try {
			return DriverManager.getConnection(url, nom, mdp);
		} catch (Exception e) {
			System.out.println(ANSI_RED + "Erreur lors de la cr�ation d'une connection � la BDD." + ANSI_RESET);
			e.printStackTrace();
		}
		return null;
	}
}
