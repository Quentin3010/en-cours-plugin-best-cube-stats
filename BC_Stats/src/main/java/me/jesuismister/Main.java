package me.jesuismister;

import org.bukkit.plugin.java.JavaPlugin;

import me.jesuismister.commands.CommandsBCStats;
import me.jesuismister.sql.DS;

public class Main extends JavaPlugin {

	@Override
	public void onEnable() {
		saveDefaultConfig();
		DS.setupDS(this);

		getCommand("bcstats").setExecutor(new CommandsBCStats());
	}

}