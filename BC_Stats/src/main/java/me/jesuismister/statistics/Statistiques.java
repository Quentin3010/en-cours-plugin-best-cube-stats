package me.jesuismister.statistics;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.Statistic;
import org.bukkit.World;
import org.bukkit.entity.EntityType;

import me.jesuismister.sql.Requetes;

public class Statistiques {

	public static List<String> getAllPlayers() {
		List<String> players = new ArrayList<String>();

		// Recuperer le monde principal
		World overworld = null;
		for (World world : Bukkit.getWorlds()) {
			if (world.getEnvironment() == World.Environment.NORMAL) {
				overworld = world;
				break;
			}
		}

		if (overworld != null) {
			// Parcourir les fichiers de sauvegarde des donnees des joueurs
			File playerDataFolder = new File(overworld.getWorldFolder(), "playerdata");
			File[] playerDataFiles = playerDataFolder.listFiles();

			// Parcourir les fichiers de donnees des joueurs et recuperer leurs noms
			if (playerDataFiles != null) {
				for (File playerDataFile : playerDataFiles) {
					String playerName = playerDataFile.getName().replace(".dat", "");
					if (playerName.length()<=36 && !playerName.contains("_old"))
						players.add(playerName);
				}
			}
		}
		return players;
	}

	public static void setMobsKillStats(String tabName, List<String> players, List<String> entityNames) {
		// Cr�ation de la table
		System.out.println("Creation de la base " + tabName + "_MobsKill");
		Requetes.createTable(tabName, "MobsKill", entityNames);

		// Ajout de toutes les statistiques des joueurs dans la table
		for (String uuid : players) {
			OfflinePlayer p = Bukkit.getOfflinePlayer(UUID.fromString(uuid));
			String requete = "INSERT INTO " + tabName + "_MobsKill VALUES('" + uuid + "','" + p.getName() + "',";
			for (String entityName : entityNames) {
				try {
					requete += "'" + p.getStatistic(Statistic.KILL_ENTITY, EntityType.valueOf(entityName)) + "',";
				} catch (Exception e) {
					requete += "'0',";
				}
			}
			requete = requete.substring(0, requete.length() - 1) + ")";
			Requetes.executeRequete(requete);
		}
	}

	public static void setKilledByModStats(String tabName, List<String> players, List<String> entityNames) {
		// Cr�ation de la table
		System.out.println("Creation de la base " + tabName + "_KilledByMobs");
		Requetes.createTable(tabName, "KilledByMobs", entityNames);

		// Ajout de toutes les statistiques des joueurs dans la table
		for (String uuid : players) {
			OfflinePlayer p = Bukkit.getOfflinePlayer(UUID.fromString(uuid));
			String requete = "INSERT INTO " + tabName + "_KilledByMobs VALUES('" + uuid + "','" + p.getName() + "',";
			for (String entityName : entityNames) {
				try {
					requete += "'" + p.getStatistic(Statistic.ENTITY_KILLED_BY, EntityType.valueOf(entityName)) + "',";
				} catch (Exception e) {
					requete += "'0',";
				}
			}
			requete = requete.substring(0, requete.length() - 1) + ")";
			Requetes.executeRequete(requete);
		}
	}

	public static void setBreakBlocksStats(String tabName, List<String> players, List<String> blockNames, int numero) {
		// Cr�ation de la table
		System.out.println("Creation de la base " + tabName + "_BreakBlocks" + numero);
		Requetes.createTable(tabName, "BreakBlocks" + numero, blockNames, true);

		// Ajout de toutes les statistiques des joueurs dans la table
		for (String uuid : players) {
			OfflinePlayer p = Bukkit.getOfflinePlayer(UUID.fromString(uuid));
			String requete = "INSERT INTO " + tabName + "_BreakBlocks" + numero + " VALUES('" + uuid + "','"
					+ p.getName() + "',";
			for (String blockName : blockNames) {
				try {
					requete += "'" + p.getStatistic(Statistic.MINE_BLOCK, Material.valueOf(blockName)) + "',";
				} catch (Exception e) {
					requete += "'0',";
				}
			}
			requete = requete.substring(0, requete.length() - 1) + ")";
			Requetes.executeRequete(requete);
		}
	}

	public static void setPlaceBlocksStats(String tabName, List<String> players, List<String> blockNames, int numero) {
		// Cr�ation de la table
		System.out.println("Creation de la base " + tabName + "_PlaceBlocks" + numero);
		Requetes.createTable(tabName, "PlaceBlocks" + numero, blockNames, true);

		// Ajout de toutes les statistiques des joueurs dans la table
		for (String uuid : players) {
			OfflinePlayer p = Bukkit.getOfflinePlayer(UUID.fromString(uuid));
			String requete = "INSERT INTO " + tabName + "_PlaceBlocks" + numero + " VALUES('" + uuid + "','"
					+ p.getName() + "',";
			for (String blockName : blockNames) {
				try {
					requete += "'" + p.getStatistic(Statistic.USE_ITEM, Material.valueOf(blockName)) + "',";
				} catch (Exception e) {
					requete += "'0',";
				}
			}
			requete = requete.substring(0, requete.length() - 1) + ")";
			Requetes.executeRequete(requete);
		}
	}

	public static void setCraftBlocksStats(String tabName, List<String> players, List<String> blockNames, int numero) {
		// Cr�ation de la table
		System.out.println("Creation de la base " + tabName + "_CraftBlocks" + numero);
		Requetes.createTable(tabName, "CraftBlocks" + numero, blockNames, true);

		// Ajout de toutes les statistiques des joueurs dans la table
		for (String uuid : players) {
			OfflinePlayer p = Bukkit.getOfflinePlayer(UUID.fromString(uuid));
			String requete = "INSERT INTO " + tabName + "_CraftBlocks" + numero + " VALUES('" + uuid + "','"
					+ p.getName() + "',";
			for (String blockName : blockNames) {
				try {
					requete += "'" + p.getStatistic(Statistic.CRAFT_ITEM, Material.valueOf(blockName)) + "',";
				} catch (Exception e) {
					requete += "'0',";
				}
			}
			requete = requete.substring(0, requete.length() - 1) + ")";
			Requetes.executeRequete(requete);
		}
	}

	public static void setCraftItemsStats(String tabName, List<String> players, List<String> itemNames) {
		// Cr�ation de la table
		System.out.println("Creation de la base " + tabName + "_CraftItems");
		Requetes.createTable(tabName, "CraftItems", itemNames, true);

		// Ajout de toutes les statistiques des joueurs dans la table
		for (String uuid : players) {
			OfflinePlayer p = Bukkit.getOfflinePlayer(UUID.fromString(uuid));
			String requete = "INSERT INTO " + tabName + "_CraftItems VALUES('" + uuid + "','" + p.getName() + "',";
			for (String itemName : itemNames) {
				try {
					requete += "'" + p.getStatistic(Statistic.CRAFT_ITEM, Material.valueOf(itemName)) + "',";
				} catch (Exception e) {
					requete += "'0',";
				}
			}
			requete = requete.substring(0, requete.length() - 1) + ")";
			Requetes.executeRequete(requete);
		}
	}

	public static void setOtherStats(String tabName, List<String> players) {
		// Cr�ation de la table
		System.out.println("Creation de la base " + tabName + "_Other");
		List<String> advancementList = new ArrayList<>();
		for (Statistic stat : Statistic.values()) {
			if (!stat.name().equals("BREAK_ITEM") && !stat.name().equals("CRAFT_ITEM") && !stat.name().equals("DROP")
					&& !stat.name().equals("ENTITY_KILLED_BY") && !stat.name().equals("KILL_ENTITY")
					&& !stat.name().equals("MINE_BLOCK") && !stat.name().equals("PICKUP")
					&& !stat.name().equals("USE_ITEM")) {
				advancementList.add(stat.name());
			}
		}
		Collections.sort(advancementList);

		Requetes.createTable(tabName, "Other", advancementList);

		// Ajout de toutes les statistiques des joueurs dans la table
		for (String uuid : players) {
			OfflinePlayer p = Bukkit.getOfflinePlayer(UUID.fromString(uuid));
			String requete = "INSERT INTO " + tabName + "_Other VALUES('" + uuid + "','" + p.getName() + "',";
			for (String statName : advancementList) {
				try {
					requete += "'" + p.getStatistic(Statistic.valueOf(statName)) + "',";
				} catch (Exception e) {
					requete += "'0',";
				}
			}
			requete = requete.substring(0, requete.length() - 1) + ")";
			Requetes.executeRequete(requete);
		}
	}

	public static void setPlayerList(String tabName, List<String> players) {
		// Cr�ation de la table
		System.out.println("Update/Creation de la base _PlayerList");

		Requetes.executeRequete("ALTER TABLE IF EXISTS _PlayerList ADD COLUMN " + tabName + " INT NULL;");
		Requetes.executeRequete("CREATE TABLE IF NOT EXISTS _PlayerList (uuid VARCHAR(255) PRIMARY KEY, pseudo TEXT, "
				+ tabName + " INT NULL);");

		// Ajout des joueurs dans la table
		String requete;
		for (String uuid : players) {
			OfflinePlayer p = Bukkit.getOfflinePlayer(UUID.fromString(uuid));
			requete = "INSERT INTO _PlayerList(uuid, pseudo, " + tabName + ") VALUES('" + uuid + "', '" + p.getName()
					+ "', 1) ON DUPLICATE KEY UPDATE " + tabName + " = 1 AND pseudo = '" + p.getName() + "';";
			Requetes.executeRequete(requete);
		}
	}
}
