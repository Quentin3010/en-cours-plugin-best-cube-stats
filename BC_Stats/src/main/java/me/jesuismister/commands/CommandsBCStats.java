package me.jesuismister.commands;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.EntityType;

import me.jesuismister.sql.Requetes;
import me.jesuismister.statistics.Statistiques;

public class CommandsBCStats implements CommandExecutor, Runnable {
	private static final String ANSI_RESET = "\u001B[0m";
	private static final String ANSI_RED = "\u001B[31m";
	private static final String ANSI_GREEN = "\u001B[32m";
	private static String argName;

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String msg, String[] args) {
		if (args.length == 2 && args[0].equalsIgnoreCase("generate")) {
			System.out.println(ANSI_GREEN + "##########################" + ANSI_RESET);
			System.out.println(ANSI_GREEN + "# Recuperation des stats #" + ANSI_RESET);
			System.out.println(ANSI_GREEN + "##########################" + ANSI_RESET);

			argName = args[1];

			Thread t = new Thread(new CommandsBCStats());
			t.start();

			return true;
		} else {
			System.out.println(ANSI_RED + "Commande invalide - bcstats generate <NOM>" + ANSI_RESET);
		}
		return false;
	}

	@Override
	public void run() {
		// Set les infos concernant la derni�re r�cup�ration des stats
		Requetes.setInfoTable(argName);

		List<String> players = Statistiques.getAllPlayers();

		// Cr�er une liste de tous les entit�es
		List<String> entityNames = new ArrayList<>();
		for (EntityType entityType : EntityType.values()) {
			entityNames.add(entityType.name());
		}
		Collections.sort(entityNames);

		// Cr�er plusieurs listes de tous les blocs
		List<String> materialList = new ArrayList<String>();
		for (Material material : Material.values()) {
			if (material.isBlock())
				materialList.add(material.name());
		}
		Collections.sort(materialList);
		int i = 0;
		List<List<String>> blockNames = new ArrayList<List<String>>();
		for (String material : materialList) {
			if (i % 400 == 0) {
				blockNames.add(new ArrayList<String>());
			}
			blockNames.get(i / 400).add(material);
			i++;
		}

		// Cr�er une liste de tous les items
		List<String> itemNames = new ArrayList<String>();
		for (Material material : Material.values()) {
			if (!material.isBlock()) {
				itemNames.add(material.name());
			}
			if (itemNames.size() == 400)
				break;
		}
		Collections.sort(itemNames);

		// Nombre de chaque entitees tuees par chaque joueur
		Statistiques.setMobsKillStats(argName, players, entityNames);

		// Nombre de chaque mort par chaque entitees par chaque joueur
		Statistiques.setKilledByModStats(argName, players, entityNames);

		// Nombre de chaque bloc casses par chaque joueur
		for (i = 0; i < blockNames.size(); i++) {
			Statistiques.setBreakBlocksStats(argName, players, blockNames.get(i), i);
		}

		// Nombre de chaque bloc poses par chaque joueur
		for (i = 0; i < blockNames.size(); i++) {
			Statistiques.setPlaceBlocksStats(argName, players, blockNames.get(i), i);
		}

		// Nombre de chaque bloc crate par chaque joueur
		for (i = 0; i < blockNames.size(); i++) {
			Statistiques.setCraftBlocksStats(argName, players, blockNames.get(i), i);
		}

		// Nombre de chaque bloc crate par chaque joueur
		Statistiques.setCraftItemsStats(argName, players, itemNames);

		// Toutes les autres statistics
		Statistiques.setOtherStats(argName, players);

		// Tous les membres de la saison
		Statistiques.setPlayerList(argName, players);

		System.out.println(ANSI_GREEN + "FINITO" + ANSI_RESET);

	}
}