package me.jesuismister.resources;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Response;
import me.jesuismister.main.ApiV1;
import me.jesuismister.main.Main;
import me.jesuismister.sql.DS;

@Produces("application/json")
@Path("craftBlocks")
public class CraftBlocksResource {
	private final static String cat = "_CraftBlocks";
	
	@GET
	@Path("{BCseason}/{uuid}")
	public Response getcraftBlocks(@PathParam("BCseason") String BCseason, @PathParam("uuid") String uuid) {
		List<List<String>> res = new ArrayList<List<String>>();
		List<String> listBloc = new ArrayList<String>();
		List<String> listStat = new ArrayList<String>();
		
		int i = 0;
		while(true) {
			String requete = "SELECT * FROM " + BCseason + cat + i + " WHERE uuid = '" + uuid + "';";
			try (Connection con = DS.getConnection()) {
				if(ApiV1.mapSeasonCol.containsKey(BCseason + cat + i)) listBloc.addAll(ApiV1.mapSeasonCol.get(BCseason + cat + i));
				else break;
				PreparedStatement ps = (PreparedStatement) con.prepareStatement(requete);
				ResultSet rs = ps.executeQuery();
				rs.next();
				for (String colName : ApiV1.mapSeasonCol.get(BCseason + cat + i)) {
					listStat.add(rs.getString(colName));
				}
				con.close();
				i++;
			} catch (Exception e) {
				Main.LOGGER.info("ERREUR : La requete a echoue (" + requete + ")");
				break;
			}
		}
		
		res.add(listBloc);
		res.add(listStat);
		return Response.ok(res).header("Access-Control-Allow-Origin", "*").build();
	}
	
	@GET
	@Path("{BCseason}/{uuid}/{search}")
	public Response getcraftBlocksWithSearch(@PathParam("BCseason") String BCseason, @PathParam("uuid") String uuid, @PathParam("search") String search) {
		List<List<String>> res = new ArrayList<List<String>>();
		List<String> listBloc = new ArrayList<String>();
		List<String> listStat = new ArrayList<String>();
		
		List<String> temp;
		
		int i = 0;
		while(true) {
			String requete = "SELECT * FROM " + BCseason + cat + i + " WHERE uuid = '" + uuid + "';";
			temp = new ArrayList<String>();
			try (Connection con = DS.getConnection()) {
				if(ApiV1.mapSeasonCol.containsKey(BCseason + cat + i)) {
					for(String name : ApiV1.mapSeasonCol.get(BCseason + cat + i)) {
						if(name.contains(search.toUpperCase())) temp.add(name);
					}
				}
				else break;
				PreparedStatement ps = (PreparedStatement) con.prepareStatement(requete);
				ResultSet rs = ps.executeQuery();
				rs.next();
				for (String colName : temp) {
					listStat.add(rs.getString(colName));
				}
				con.close();
				
				listBloc.addAll(temp);
				i++;
			} catch (Exception e) {
				Main.LOGGER.info("ERREUR : La requete a echoue (" + requete + ")");
				break;
			}
		}
		
		res.add(listBloc);
		res.add(listStat);
		return Response.ok(res).header("Access-Control-Allow-Origin", "*").build();
	}
}
