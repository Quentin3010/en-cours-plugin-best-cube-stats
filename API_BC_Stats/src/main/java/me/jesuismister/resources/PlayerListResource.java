package me.jesuismister.resources;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Response;
import me.jesuismister.main.Main;
import me.jesuismister.sql.DS;

@Produces("application/json")
@Path("playerList")
public class PlayerListResource {

	@GET
	@Path("{BCseason}")
	public Response getPlayerList(@PathParam("BCseason") String BCseason) {
		List<String[]> res = new ArrayList<String[]>();
		String cat = "_PlayerList";

		String requete = "SELECT * FROM " + cat + " WHERE " + BCseason + "=1 ORDER BY pseudo;";
		try (Connection con = DS.getConnection()) {
			PreparedStatement ps = (PreparedStatement) con.prepareStatement(requete);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				res.add(new String[]{rs.getString("uuid"), rs.getString("pseudo")});
			}
			con.close();
		} catch (Exception e) {
			Main.LOGGER.info("ERREUR : La requete a echoue (" + requete + ")");
		}
		return Response.ok(res).header("Access-Control-Allow-Origin", "*").build();
	}
}
