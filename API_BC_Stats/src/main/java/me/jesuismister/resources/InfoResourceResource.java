package me.jesuismister.resources;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Response;
import me.jesuismister.main.ApiV1;
import me.jesuismister.main.Main;
import me.jesuismister.sql.DS;

@Produces("application/json")
@Path("info")
public class InfoResourceResource {

	@GET
	@Path("{BCseason}/lastUpdate")
	public Response getLastUpdate(@PathParam("BCseason") String BCseason) {
		String res = "ERROR";
		String requete = "SELECT lastUpdate FROM  _Informations WHERE BCSeason = '" + BCseason + "';";
		try (Connection con = DS.getConnection()) {
			PreparedStatement ps = (PreparedStatement) con.prepareStatement(requete);
			ResultSet rs = ps.executeQuery();
			rs.next();
			res = rs.getString("lastUpdate");
			con.close();
		} catch (Exception e) {
			Main.LOGGER.info("ERREUR : La requete a echoue (" + requete + ")");
		}
		return Response.ok(List.of(res)).header("Access-Control-Allow-Origin", "*").build();
	}

	@GET
	@Path("allSeason")
	public Response getAllSeason() {
		List<String> res = new ArrayList<String>();
		String requete = "SELECT BCSeason FROM  _Informations;";
		try (Connection con = DS.getConnection()) {
			PreparedStatement ps = (PreparedStatement) con.prepareStatement(requete);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				res.add(rs.getString("BCSeason"));
			}
			Collections.sort(res);
			con.close();
		} catch (Exception e) {
			Main.LOGGER.info("ERREUR : La requete a echoue (" + requete + ")");
		}
		return Response.ok(res).header("Access-Control-Allow-Origin", "*").build();
	}

	@GET
	@Path("allSeason/{uuid}")
	public Response getAllSeason(@PathParam("uuid") String uuid) {
		List<String> res = new ArrayList<String>();
		
		String requete = "SELECT * FROM  _PlayerList WHERE uuid = '" + uuid + "';";
		try (Connection con = DS.getConnection()) {
			PreparedStatement ps = (PreparedStatement) con.prepareStatement(requete);
			ResultSet rs = ps.executeQuery();
			rs.next();
			for(String season : ApiV1.mapSeasonCol.get("_PlayerList")) {
				if(!season.equals("uuid") && !season.equals("pseudo") && rs.getInt(season)==1) {
					res.add(season);
				}
			}
			Collections.sort(res);
			con.close();
		} catch (Exception e) {
			Main.LOGGER.info("ERREUR : La requete a echoue (" + requete + ")");
		}
		return Response.ok(res).header("Access-Control-Allow-Origin", "*").build();
	}

	@GET
	@Path("pseudo/{uuid}")
	public Response getPseudo(@PathParam("uuid") String uuid) {
		List<String> res = new ArrayList<String>();
		String requete = "SELECT pseudo FROM  _PlayerList WHERE uuid='" + uuid + "';";
		try (Connection con = DS.getConnection()) {
			PreparedStatement ps = (PreparedStatement) con.prepareStatement(requete);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				res.add(rs.getString("pseudo"));
			}
			Collections.sort(res);
			con.close();
		} catch (Exception e) {
			Main.LOGGER.info("ERREUR : La requete a echoue (" + requete + ") ");
		}
		return Response.ok(res).header("Access-Control-Allow-Origin", "*").build();
	}
}
