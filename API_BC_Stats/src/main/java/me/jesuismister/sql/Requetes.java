package me.jesuismister.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;

public class Requetes {
	private static final String ANSI_RESET = "\u001B[0m";
    private static final String ANSI_RED = "\u001B[31m";

    public static void executeRequete(String requete) {
    	try (Connection con = DS.getConnection()) {
			PreparedStatement ps = (PreparedStatement) con.prepareStatement(requete);
			ps.executeUpdate();
			con.close();
		} catch (Exception e) {
			System.out.println(ANSI_RED + "ERREUR : " + requete + ANSI_RESET);
			e.printStackTrace();
		}
	}
}
