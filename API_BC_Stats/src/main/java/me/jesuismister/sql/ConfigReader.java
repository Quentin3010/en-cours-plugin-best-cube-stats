package me.jesuismister.sql;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class ConfigReader {
    private static String filePath;
    private static Map<String, String> configMap;

    public static void setConfigReader(String filePath) throws IOException {
    	ConfigReader.filePath = filePath;
        ConfigReader.configMap = new HashMap<>();
        readConfig();
    }

    public static void readConfig() throws IOException {
        try (BufferedReader br = new BufferedReader(new FileReader(filePath))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] parts = line.split("=");
                if (parts.length >= 2) {
                    String key = parts[0].trim();
                    String value = parts[1].trim();
                    configMap.put(key, value);
                }
            }
        }catch (FileNotFoundException e) {
        	try (BufferedWriter bw = new BufferedWriter(new FileWriter(filePath))) {
        		bw.write("ip_api=localhost");
                bw.newLine();
                bw.write("ip_bdd=localhost");
                bw.newLine();
                bw.write("nom_base=NomBase");
                bw.newLine();
                bw.write("login=login");
                bw.newLine();
                bw.write("mdp=mdp");
                bw.newLine();
                readConfig();
            }
		}
    }

    public static String getIp_API() {
        return configMap.get("ip_api");
    }
    
    public static String getIp_BDD() {
        return configMap.get("ip_bdd");
    }

    public static String getNomBase() {
        return configMap.get("nom_base");
    }

    public static String getLogin() {
        return configMap.get("login");
    }

    public static String getMdp() {
        return configMap.get("mdp");
    }
}
