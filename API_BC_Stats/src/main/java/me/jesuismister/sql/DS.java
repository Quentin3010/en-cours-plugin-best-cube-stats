package me.jesuismister.sql;

import java.sql.Connection;
import java.sql.DriverManager;

import me.jesuismister.main.Main;

public class DS {
    private static final String ANSI_RESET = "\u001B[0m";
    private static final String ANSI_RED = "\u001B[31m";
    
    private static String url;
    private static String nom;
    private static String mdp;

    public static boolean setupDS() {
        try {
        	ConfigReader.setConfigReader(System.getProperty("user.dir") + "/config.config");
            url = "jdbc:mysql://" + ConfigReader.getIp_BDD() + "/" + ConfigReader.getNomBase();
            nom = ConfigReader.getLogin();
            mdp = ConfigReader.getMdp();
            Main.BASE_URI = "http://" + ConfigReader.getIp_API() + ":8080/api/v1/";
            
            //Installation du driver
            Class.forName("com.mysql.cj.jdbc.Driver");
            
            return true;
        } catch (Exception e) {
            System.out.println(ANSI_RED + "Erreur lors de la mise en place de la relation avec la base de donnee." + ANSI_RESET);
            e.printStackTrace();
        }
        return false;
    }

    public static Connection getConnection() {
        try {
            return DriverManager.getConnection(url, nom, mdp);
        } catch (Exception e) {
        	System.out.println(ANSI_RED + "Erreur lors de la creation d'une connection a la BDD." + ANSI_RESET);
            e.printStackTrace();
        }
        return null;
    }
}
