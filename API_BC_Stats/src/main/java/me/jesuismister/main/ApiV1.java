package me.jesuismister.main;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.glassfish.jersey.server.ResourceConfig;

import jakarta.ws.rs.ApplicationPath;
import me.jesuismister.resources.BreakBlocksResource;
import me.jesuismister.resources.CraftBlocksResource;
import me.jesuismister.resources.CraftItemsResource;
import me.jesuismister.resources.InfoResourceResource;
import me.jesuismister.resources.KilledByMobsResource;
import me.jesuismister.resources.MobsKillResource;
import me.jesuismister.resources.OtherResource;
import me.jesuismister.resources.PlaceBlocksResource;
import me.jesuismister.resources.PlayerListResource;
import me.jesuismister.sql.ConfigReader;
import me.jesuismister.sql.DS;

@ApplicationPath("api/v1/")
public class ApiV1 extends ResourceConfig {
	public static Map<String, List<String>> mapSeasonCol;

	public ApiV1() {
		packages("me.jesuismister.main");

		mapSeasonCol = new HashMap<String, List<String>>();
		for (String baseName : getTablesNames()) {
			mapSeasonCol.put(baseName, new ArrayList<String>());
			for (String colName : getColsName(baseName)) {
				mapSeasonCol.get(baseName).add(colName);
			}
		}

		register(InfoResourceResource.class);
		register(BreakBlocksResource.class);
		register(CraftBlocksResource.class);
		register(CraftItemsResource.class);
		register(KilledByMobsResource.class);
		register(MobsKillResource.class);
		register(OtherResource.class);
		register(PlaceBlocksResource.class);
		register(PlayerListResource.class);
	}

	public List<String> getColsName(String nameTable) {
		List<String> res = new ArrayList<String>();
		String requete = "SELECT column_name FROM information_schema.columns WHERE table_name = '" + nameTable + "';";
		try (Connection con = DS.getConnection()) {
			PreparedStatement ps = (PreparedStatement) con.prepareStatement(requete);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				res.add(rs.getString("column_name"));
			}
			con.close();
		} catch (Exception e) {
			Main.LOGGER.info("ERREUR : La requete a echoue (" + requete + ")");
		}
		return res;
	}

	public List<String> getTablesNames() {
		List<String> res = new ArrayList<String>();
		String requete = "SELECT table_name FROM information_schema.tables WHERE table_type = 'BASE TABLE' AND table_schema = '"
				+ ConfigReader.getNomBase() + "'";
		try (Connection con = DS.getConnection()) {
			PreparedStatement ps = (PreparedStatement) con.prepareStatement(requete);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				res.add(rs.getString("table_name"));
			}
			con.close();
		} catch (Exception e) {
			Main.LOGGER.info("ERREUR : La requete a echoue (" + requete + ")");
		}
		return res;
	}
}
